﻿function showImage(imgNum) {
    $("#modalBody").html("<img class='img-responsive' src='Images/" + imgNum + ".JPG'>");
}
function showPage(pageNum) {

    switch (pageNum) {
        case 1://Home
            fadeOut();
            $("#HomeH").addClass("active");
            $("#Home").fadeIn();
            break;
        case 2://Web
            fadeOut();
            $("#SummerH").addClass("active");
            $("#Summer").fadeIn();
            break;
        case 3://Services
            fadeOut();
            $("#EventsH").addClass("active");
            $("#Events").fadeIn();
            break;
        case 4: //contact
            fadeOut();
            $("#AOSH").addClass("active");
            $("#AOS").fadeIn();
            break;
        case 5: //contact
            fadeOut();
            $("#AboutH").addClass("active");
            $("#About").fadeIn();
            break;
        case 6: //contact
            fadeOut();
            $("#ContactH").addClass("active");
            $("#Contact").fadeIn();
            break;
        default:
            break;
    }
}
function fadeOut() {
    $(".active").removeClass("active");
    $("#Home").hide();
    $("#Summer").hide();
    $("#Events").hide();
    $("#AOS").hide();
    $("#About").hide();
    $("#Contact").hide();
}